import dask.dataframe as da
import altair as alt
import pandas as pd

DEPARTEMENT = '044'
COMMUNE = 'GETIGNE'

df = da.read_csv('comptes-individuels-des-communes-fichier-global-a-compter-de-2000.csv', sep=';', dtype={'dep': 'string','remp': 'object',
       'rfctva': 'object',
       'rraff': 'object',
       'rsubr': 'object'})

df_dep = df[df.dep==DEPARTEMENT].compute()
step = 1
overlap = 1
base = alt.Chart(
    df_dep,
    width=1600,
    height=50
).transform_density(
    'fcaf',
    groupby=['an'],
    as_=['fcaf_density', 'density'],
    extent=[-100, 600],
).mark_area(
    interpolate='monotone',
    fillOpacity=0.8,
    stroke='lightgray',
    strokeWidth=0.5
).encode(
    x=alt.X("fcaf_density:Q", title="Capacité d'autofinancement des communes du 44 (€/hab)"),
    y=alt.Y('density:Q').axis(None),#.scale(range=[step, -step * overlap]),
    tooltip=['dep', 'inom', 'caf', 'fcaf', 'mcaf'],
)

rule = alt.Chart(
    df_dep
).transform_filter(
    alt.FieldEqualPredicate(field='inom', equal=COMMUNE)
).mark_rule(color="red").encode(
    x='fcaf',
    tooltip=['dep', 'inom', 'caf', 'fcaf', 'mcaf'],
)

(base + rule).facet(
    row=alt.Row('an:N')
    .title(None)
    .header(labelAngle=0, labelAlign='left')
).properties(
    title="Densité de la capacité d'autofinancement des communes du 44 entre 2000 et 2022",
    bounds='flush'
).configure_facet(
    spacing=0
).configure_view(
    stroke=None
).configure_title(
    anchor='end'
).interactive().save('44_caf_density.html')
